import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage({
  name: 'Home',
  priority: 'high'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {

   public hasComics     : boolean = false;
   public comics        : any;

   constructor( public navCtrl    : NavController,
              	public DB          : DatabaseProvider)
   {

   }
   ionViewWillEnter()
   {
      this.displayComics();
   }

   displayComics()
   {
      this.DB.retrieveComics().then((data)=>
      {
         let existingData = Object.keys(data).length;

         console.log(existingData);

         if(existingData !== 0)
   {
            this.hasComics  = true;
            this.comics     = data;

            console.log(this.comics);
   }
   else
   {
      console.log("we get nada!");
   }
      });
   }


   addCharacter()
   {
      this.navCtrl.push('Add');
   }


   viewCharacter(param)
   {
      this.navCtrl.push('Add', param);
      console.log(param);
   }

}