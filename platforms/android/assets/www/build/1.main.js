webpackJsonp([1],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add__ = __webpack_require__(278);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddModule", function() { return AddModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddModule = (function () {
    function AddModule() {
    }
    return AddModule;
}());
AddModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__add__["a" /* Add */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__add__["a" /* Add */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__add__["a" /* Add */]
        ]
    })
], AddModule);

//# sourceMappingURL=add.module.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_image_image__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(200);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Add; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Add = (function () {
    function Add(navCtrl, NP, fb, IMAGE, DB, toastCtrl) {
        this.navCtrl = navCtrl;
        this.NP = NP;
        this.fb = fb;
        this.IMAGE = IMAGE;
        this.DB = DB;
        this.toastCtrl = toastCtrl;
        this.isEdited = false;
        this.hideForm = false;
        this.form = fb.group({
            "character": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required],
            "title": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required],
            "rating": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required],
            "image": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required],
            "note": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* Validators */].required]
        });
        this.resetFields();
        if (NP.get("key") && NP.get("rev")) {
            this.recordId = NP.get("key");
            this.revisionId = NP.get("rev");
            this.isEdited = true;
            this.selectComic(this.recordId);
            this.pageTitle = 'Amend entry';
        }
        else {
            this.recordId = '';
            this.revisionId = '';
            this.isEdited = false;
            this.pageTitle = 'Create entry';
        }
    }
    Add.prototype.selectComic = function (id) {
        var _this = this;
        this.DB.retrieveComic(id)
            .then(function (doc) {
            _this.comicCharacter = doc[0].character;
            _this.comicTitle = doc[0].title;
            _this.comicRating = doc[0].rating;
            _this.comicNote = doc[0].note;
            _this.comicImage = doc[0].image;
            _this.characterImage = doc[0].image;
            _this.recordId = doc[0].id;
            _this.revisionId = doc[0].rev;
        });
    };
    Add.prototype.saveComic = function () {
        var _this = this;
        var character = this.form.controls["character"].value, title = this.form.controls["title"].value, rating = this.form.controls["rating"].value, image = this.form.controls["image"].value, note = this.form.controls["note"].value, revision = this.revisionId, id = this.recordId;
        if (this.recordId !== '') {
            this.DB.updateComic(id, title, character, rating, note, image, revision)
                .then(function (data) {
                _this.hideForm = true;
                _this.sendNotification(character + " was updated in your comic characters list");
            });
        }
        else {
            this.DB.addComic(title, character, rating, note, image)
                .then(function (data) {
                _this.hideForm = true;
                _this.resetFields();
                _this.sendNotification(character + " was added to your comic characters list");
            });
        }
    };
    Add.prototype.takePhotograph = function () {
        var _this = this;
        this.IMAGE.takePhotograph()
            .then(function (image) {
            _this.characterImage = image.toString();
            _this.comicImage = image.toString();
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    Add.prototype.selectImage = function () {
        var _this = this;
        this.IMAGE.selectPhotograph()
            .then(function (image) {
            _this.characterImage = image.toString();
            _this.comicImage = image.toString();
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    Add.prototype.deleteComic = function () {
        var _this = this;
        var character;
        this.DB.retrieveComic(this.recordId)
            .then(function (doc) {
            character = doc[0].character;
            return _this.DB.removeComic(_this.recordId, _this.revisionId);
        })
            .then(function (data) {
            _this.hideForm = true;
            _this.sendNotification(character + " was successfully removed from your comic characters list");
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    Add.prototype.resetFields = function () {
        this.comicTitle = "";
        this.comicRating = "";
        this.comicCharacter = "";
        this.comicNote = "";
        this.comicImage = "";
        this.characterImage = "";
    };
    Add.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    return Add;
}());
Add = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
        selector: 'page-add',template:/*ion-inline-start:"/home/bryan/pouchdb_comics/src/pages/add/add.html"*/'<ion-header>\n   <ion-navbar>\n      <ion-title>{{ pageTitle }}</ion-title>\n   </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n   <div>\n      <ion-item *ngIf="isEdited && !hideForm">\n         <button\n            ion-button\n      item-right\n      color="secondary"\n      text-center\n      block\n      (click)="deleteComic()">Remove this Entry?</button>\n      </ion-item>\n\n\n      <div *ngIf="hideForm">\n         <ion-item class="post-entry-message" text-wrap>\n            <h2>Success!</h2>\n       <p>Maybe you\'d like to edit an existing entry or add a new record?</p>\n       <p>Simply go back to the home page and select the option you want to pursue.</p>\n         </ion-item>\n      </div>\n\n\n\n      <div *ngIf="!hideForm">\n         <form [formGroup]="form" (ngSubmit)="saveComic()">\n\n            <ion-list>\n               <ion-item-group>\n                  <ion-item-divider color="light">Publication Name</ion-item-divider>\n                  <ion-item>\n                     <ion-label>Please select: </ion-label>\n                     <ion-select\n                        class="select"\n      interface="action-sheet"\n      formControlName="title"\n            block\n      [(ngModel)]="comicTitle">\n      <ion-option value="Battle/Action Force">Battle/Action Force</ion-option>\n      <ion-option value="Eagle">Eagle</ion-option>\n      <ion-option value="2000AD">2000AD</ion-option>\n      <ion-option value="Scream">Scream</ion-option>\n      <ion-option value="Other">Other</ion-option>\n                     </ion-select>\n                  </ion-item>\n               </ion-item-group>\n\n\n               <ion-item-group>\n                  <ion-item-divider color="light">Character Name</ion-item-divider>\n                  <ion-item>\n                     <ion-input\n                        type="text"\n                        placeholder="Enter a name..."\n                        formControlName="character"\n                        [(ngModel)]="comicCharacter"></ion-input>\n                  </ion-item>\n               </ion-item-group>\n\n\n               <ion-item-group>\n                  <ion-item-divider color="light">Character Image</ion-item-divider>\n                  <ion-item>\n                     <a\n                        ion-button\n      block\n      margin-bottom\n      color="primary"\n      (click)="takePhotograph()">\n         Take a photograph\n                     </a>\n                  </ion-item>\n\n                  <ion-item>\n                     <a\n                        ion-button\n      block\n      margin-bottom\n      color="secondary"\n      (click)="selectImage()">\n         Select an existing image\n                     </a>\n                  </ion-item>\n\n                  <ion-item>\n                     <img [src]="characterImage">\n          <input type="hidden" name="image" formControlName="image" [(ngModel)]="comicImage">\n                  </ion-item>\n               </ion-item-group>\n\n\n\n               <ion-item-group>\n                  <ion-item-divider color="light">Character Rating</ion-item-divider>\n                  <ion-item>\n                     <ion-label text-left>Rating for this character?</ion-label>\n                     <ion-range\n                        class="textarea"\n      formControlName="rating"\n      min="1"\n      max="5"\n      step="1"\n      snaps="true"\n      secondary\n      [(ngModel)]="comicRating">\n                        <ion-label range-left>1</ion-label>\n      <ion-label range-right>5</ion-label>\n                     </ion-range>\n                  </ion-item>\n               </ion-item-group>\n\n\n               <ion-item-group>\n                  <ion-item-divider color="light">Character Description</ion-item-divider>\n                  <ion-item>\n                     <ion-textarea\n                        placeholder="Additional notes..."\n      formControlName="note"\n      rows="6"\n      [(ngModel)]="comicNote"></ion-textarea>\n                  </ion-item>\n               </ion-item-group>\n\n\n               <ion-item>\n                  <button\n                     ion-button\n         color="primary"\n         text-center\n         block\n         [disabled]="!form.valid">Save Entry</button>\n               </ion-item>\n\n\n\n            </ion-list>\n\n\n\n         </form>\n      </div>\n   </div>\n\n\n</ion-content>'/*ion-inline-end:"/home/bryan/pouchdb_comics/src/pages/add/add.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_image_image__["a" /* ImageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
], Add);

//# sourceMappingURL=add.js.map

/***/ })

});
//# sourceMappingURL=1.main.js.map